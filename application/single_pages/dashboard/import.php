<h1>Importation des contenus wordpress</h1>
<?php
#########################################################################
# script for importing content                                          #
#                                                                       #
# Fred Radeff (aka FR), fradeff@akademia.ch, radeff.red                 #
# credits: thanks to Karl for contributions                             #
# required: concrete5.8, ad hoc MySQL table with  content to import     #
# usage: create MySQL table with your datas, comment line 14 and run    #
# the single page                                                       #
# prior to run this script, create your topics using addtags.php        #
# hint: create a page attribute with the id of the record (here wpid)   #
#########################################################################
echo "disabled, ask website's administrator for activating"; exit;

#########################################################################
use \Concrete\Core\Tree\Type\Topic as TopicTree;
use \Concrete\Core\Tree\Node\Node as TreeNode;
use \Concrete\Core\Tree\Node\Type\Topic as TopicTreeNode;
use \Concrete\Core\Attribute\Type as AttributeType;

$servername= "localhost";
$username="wpDbdlcmtools";
$password="wpDbdlcmtools";
$namedb="wpDbdlcmtools";

		try {

			$connexion = new PDO("mysql:host=$servername; dbname=" . $namedb, $username, $password);
            // setting the PDO error mode to exception
            $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//            $sql ="SELECT * FROM `Dlcmresearch` ORDER BY `Dlcmresearch`.`idtool` ASC LIMIT 0,100";

            //$sql ="SELECT * FROM `Dlcmresearch` WHERE `Dlcmresearch`.`idtool` = 676";
          //  $sql ="SELECT * FROM `Dlcmresearch` WHERE `Dlcmresearch`.`idtool` = 539";
// $sql ="SELECT * FROM `Dlcmresearch` WHERE `Dlcmresearch`.`idtool` = 534";
$sql ="SELECT * FROM `Dlcmresearch` ORDER BY `Dlcmresearch`.`idtool` ASC";

            $stmt = $connexion->prepare($sql);

            //$connexion->bindParam(':lechamp', $champ);
            $stmt->execute();
            foreach ($connexion->query($sql) as $row) {
                		$id=$row['wp-post_id'];
										//contenu original
										$billet=nettoie($row['content-encoded']);
										$introtext=substr($billet, 0,100);
										$introtext=preg_replace("/<.*/","", $introtext);
										if(strlen($introtext)>0){
											$introtext=$introtext."...";
										}
                echo("\n...........#" .$id ." " . $row["idtool"] ."  ......." .$row['title'] ) ;
                $parentPage = \Page::getByPath('/blog');

                $pageType = \PageType::getByHandle('blog_entry');
                //Then, choose a page template for the new page.
                $template = \PageTemplate::getByHandle('right_sidebar');

                $newPage = $parentPage->add($pageType, array(
                    //  'cName' => htmlentities($titre),
                      'cName' => $row['title'],
											'cDescription' => $introtext,
                      /*To show after
c5_dlcmtools.sql
                        'cHandle ' => $alias,
                      */
                        'cDatePublic' => $row['wp-post_date'] // Note the format
                    ), $template);

                 	echo "<p>id: $id</p>";
		$newPage->setAttribute('wpid', $id); // old wordpress ID


   //import tags
		$post_tag=$row['post_tag'];
		$post_tag=explode(' | ', $post_tag);
		//do only if there is a tag
		$ak = CollectionAttributeKey::getByHandle('blog_entry_topics');
		$akHandle = $ak->getAttributeKeyHandle();
		//astuce: créer un array
		$tagValues = array();
		if(count($post_tag)>0 && $post_tag){

		for ($pt=0; $pt<count($post_tag); $pt++){
				$letag=$post_tag[$pt];
				$letag=trim($letag);
				//only if there is content in the tag
				if(strlen($letag)>0){
				echo "<br>add tag: " .$letag;
				$topic = TopicTreeNode::getNodeByName($letag);
				if(!is_null($topic)) {
//				if($topic->getTreeNodeDisplayPath() != null) {
//					if($topic->getTreeNodeDisplayPath()) {
//					if(!is_null($topic->getTreeNodeDisplayPath())) {
					$tagValues[] = $topic->getTreeNodeDisplayPath(); //on pousse dans l'array
				}
				}
		}
		$newPage->setAttribute($akHandle, $tagValues);
	}
		//write content to content block on the created page
        //content-encoded

				$contentBlock = BlockType::getByHandle('content');
				$fulltext='<h1>' .$row['title'] .'</h1>' .$fulltext;
				$fulltext="";
				$fulltext.='<h1>' .$row['title'] .'</h1>';
				$publisher = trim($row['front_firstname']) ." " .trim($row['front_lastname']);
				//$fulltext .= info_publications("Initial WP post: ", $row['wp-post_id'], " Publié le: ",
				$fulltext .= info_publications("",""," Published: ",
				$row['pubDate'], " by: ", $publisher, "publication");
				$fulltext .= disposetext($row['ressources_category'], $row['toolsandservices'],"Ressources Category: ", "catressources") ;
				//disposetext($text, $toolsandserv, $titre="", $css_mainclass ="res-categ", $removeguillemet=true)
				$fulltext .= disposetext($row['contraintes'], "", "Constraints: ", "contraints") ;

				////////////////////////////////////////////////////////////////
				//Adding here if existing Licence, languages, platform and contraints

				$fulltext .= multiplecheckbox($row['licence'], "Licence form: ", "licence");
				$fulltext .= multiplecheckbox($row['languages'], "Available languages: ", "language", true);
				$fulltext .= multiplecheckbox($row['platform'], "Platform: ", "platform");

				//How to mention the different web links used
				$fulltext .= weblinkbuild($row['web_links_0_link'], $row['web_links_1_link'],
										  $row['web_links_2_link'], $row['web_links_3_link'],
										  "Useful Links:", "weblink"
										);
				$fulltext .= "<br/>" ;
				$fulltext .= $billet;
				$fulltext .= add_comments("General Comments: ", nettoie($row['general_comments']), "css_comments");

        $blockData = array('content' => $fulltext);

        // Note that this is case sensitive and has to be EXACTLY the same as the area name you use in your template.
        $areaName = 'Main';
        $newBlock = $newPage->addBlock($contentBlock, $areaName, $blockData);
        $newBlock->updateBlockInformation($blockData);

			}
		}

		catch (PDOException $e) {
			//echo "\nError: Unable to create the database table " ;
			echo  "\nerror sql query import: " . $e->getMessage();
			die();
		}

		$connexion = null;

		//exit;

		//echo $sql;


//exit;

/*

##########  TOOLS & FUNCTIONS ################

*/

function nettoie($string) {
	/*	$string=preg_replace("/\'/","’",$string);
		$string=preg_replace("/\'/","’",$string);
		$string=preg_replace("/\'/","’",$string);
		//$string=str_replace("’","&#145;",$string);
		//$string=str_replace("''","&#145;",$string);
		$string=str_replace("’","'",$string);

		$string= str_replace (chr(146),"'",$string) ;
		$string= str_replace (chr(145),"'",$string) ;
		//$string=utf8_encode($string);
		$string= str_replace ('src="data/images','src="http://www.uniterre.ch/zjoom/data/images',$string) ;
		$string= str_replace ('href="data/docs/','href="/zjoom/data/docs/',$string) ;
		//some stange replacements (. d'interrogations)
		$string= str_replace ('?ha','ha',$string) ;
		$string= str_replace ('?-','-',$string) ;
		$string= str_replace ('-?','-',$string) ;
		$string= str_replace ('–','-',$string) ;
		 $string= str_replace ('…','...',$string) ;
		 $string= str_replace ('œ','oe',$string) ;

		//return utf8_decode($string);*/
		$string=preg_replace("/.caption.*\/caption./","",$string);
		//case [pdf-embedder url="http://www.dlcm.ch/wp-content/uploads/2016/06/RDM_Policy_Outline_20160518_v1.0.pdf"]
		$string=preg_replace("/\[pdf-embedder url..(.*).]/","$1",$string);
		if(preg_match("/http/",$string) && !preg_match("/a href/",$string)){
			preg_replace('/((www|http:\/\/)\S+)/', '<a href="$1">$1</a>', $string);
			preg_replace('/((https:\/\/)\S+)/', '<a href="$1">$1</a>', $string);
		}
		return $string;
	}
	function multiplecheckbox($text, $checkboxtitle, $css_genericname, $removeguillemet = false){
		//checkbox( string $key, string $value, string $isChecked = false, array $miscFields = array() )

		if(trim($text) === "") return "";

		$css_title = $css_genericname . "-title";  //Create the CSS class name for the title if mentioned
		$css_boxes = $css_genericname . "-boxes";  //Create the CSS class name for the title if mentioned


		$phr  = "<div>";  //HTML Div for this part of the series of checked boxes

		//Write  the label /title of these series of checked boxes, & mention its CSS class
		if(trim($checkboxtitle) !== ""){

			$phr .= '<span class="' .$css_title  .'">' .$checkboxtitle . '</span>';
		}

		$ars = explode("|",$text);   //Extract each element separated by | character

		foreach($ars as $element){


			if($removeguillemet)
				$elem = trim(str_replace('"', " ", $element));
			else
				$elem = trim($element);

			//Each box mentioned is already checked, the boxes will be displayed in line Horizontally
			$phr .= '<INPUT TYPE="Checkbox" class="' .$css_boxes . '" Value="' .$elem .'" Checked> ' .$elem .'&nbsp';
		}

		$phr  .= "</div><br/>";

	    return $phr;
	}





function weblinkbuild($link0, $link1, $link2, $link3, $titlesection, $css_rootclassname="links"){

	$array_links = array(fullStrings($link0), fullStrings($link1), fullStrings($link2), fullStrings($link3));

	$buildweb = $array_links[0] || $array_links[1] || $array_links[2] || $array_links[3] ;

	if($buildweb){

		$ph  = '<div class="' . $css_rootclassname .'">';
		$isthere_atitle = false;

		if(fullStrings($titlesection)) {
			$isthere_atitle = true;
			$classtitle =  $css_rootclassname ."-title"; //Convention Add "-title" to this root name section

			$ph .= enGlobe('<h2>' .$titlesection .'</h2>',  $classtitle , "div");
		}

		$ph .= '<span class="' . $css_rootclassname .'-weblinks">';

		for($i=0; $i<4; $i++){

			if($array_links[$i]){
				$weblnk = ${"link" .$i};
				$ph .= '<br/><a href="' .$weblnk .'">' .$weblnk .'</a>';
			}
		}

		$ph .= '</span>';  //Closing div of css_rootclassname -weblinks

		//Don't forget to close <divs> of "thelinks-weblinks" &  "thelinks" section
		//Add a New Line to separate the following content

		if($isthere_atitle) $ph .= '</div>'; //Closing </div> of the title of WEb links if mentioned

		return $ph;
	}

	// if no single weblink was mentioned then return NULL string
	return "";
}

function fullStrings(&$str) {

	if(isset($str)) {

		$str = trim($str);
		return (strlen($str) > 0);
	}
	return false;
  }

function info_publications($wpIDpostitre, $wpIDpost, $publishdatetitl, $publishdate, $publishBytitle,
						    $publishBy, $css_mainclass)
{
	$css_title = $css_mainclass . "-title";
	$divsectionpresent = false;

	if(fullStrings($wpIDpostitre)){
		$ph = '<div class="' .$css_mainclass .'">';

		$ph .= enGlobe($wpIDpostitre, $css_title, "span") .$wpIDpost ;

		$divsectionpresent = true;
	}


	if(fullStrings($publishdate)){

		if(!$divsectionpresent) $ph .= '<div class="' .$css_mainclass .'">' ;

		$ph  .= enGlobe($publishdatetitl, $css_title, "span") .substr($publishdate, 5, 11);

		if(fullStrings($publishBy)){
			$ph  .= enGlobe($publishBytitle, $css_title, "span") .$publishBy ;
		}
	}

	if(!$divsectionpresent) $ph .= '</div>';

	return $ph .'<br/>';
}

function add_comments($postitre, $textpost, $css_mainclass) {

	if(fullStrings($textpost)){
		$ph = '<br/><br/><div class="' .$css_mainclass .'">';

		$css_title = $css_mainclass . "-title";
		$ph .= enGlobe($postitre, $css_title, "div") ;

		$css_contenu = $css_mainclass . "-content";
		$ph .=  enGlobe($textpost, $css_mainclass, "div") .'</div>' ;

		return $ph;
	}

return "";
}

function enGlobe($text, $laclasse, $balise){
	return '<' .$balise . ' class="' .$laclasse .'">' .$text .'</' .$balise .'>';
}

function disposetext($text, $toolsandserv, $titre="", $css_mainclass ="res-categ", $removeguillemet=true)
{
	//This function will add ressources category in case the corresponding field is Not NULL

	$css_boxes = $css_mainclass . "-boxes";
	$isthere_ressourcecateg = false;

	$ph = "";

	if(fullStrings($text)){
		$isthere_ressourcecateg = true;
		// Add a Title (in bold characters) with its own CSS class
		$ph  .= '<div class="' .$css_mainclass .'">';

		if($titre !== ""){
			$classname =  $css_mainclass . "-title";
			$ph .= enGlobe($titre, $classname, "span") ;
		}

		//Content of Ressources Category replace all hyphens with spaces from the text
		$ph .= explode_toBoxes($text, $css_boxes, $removeguillemet, true);
	}
	//Mention if there is Tools and services between brackets with checked boxes Tools , Services between ()
	if(fullStrings($toolsandserv)){

		if(!$isthere_ressourcecateg) //Put the class name in the beginnning in No  Ressources Categ is entered
			$ph  .= '<div class="' .$css_mainclass .'">';

		/////////////////////////////////////////////
		//Put Checkboxes before Tools and Services if mentioned
		$ph  .=  " ( " .explode_toBoxes($toolsandserv, $css_boxes, true, false) . " )";

	}

	if(strlen($ph) < 1) return "";

	return $ph.'</div>';

}

function explode_toBoxes($text, $css_boxes, $removeguillemet, $removehyphen = true){


	$ph = "";

	$ars = explode("|",$text);   //Extract each element separated by | character

	foreach($ars as $element){

		$elem = trim($element);

		if($removeguillemet) {
			$elem = str_replace('"', " ", $element);
		}

		//Each box mentioned is already checked, the boxes will be displayed in line Horizontally
		$ph .= '<INPUT TYPE="Checkbox" class="' .$css_boxes . '" Value="' .$elem .'" Checked> ';

		if($removehyphen) $elem = str_replace("-", " ", $elem);

		$ph .=  $elem .'&nbsp';
	}

	return $ph;
}




 ?>
